package com.nhauindaba.app.ui;

import android.support.v4.app.Fragment;

public class ArticleFragmentActivity extends BaseFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return ArticleFragment.newInstance();
	}

}
