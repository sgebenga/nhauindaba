package com.nhauindaba.app.ui;

import android.support.v4.app.Fragment;
import android.view.Menu;

public class HeadlinesFragmentActivity extends BaseFragmentActivity {

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.headlines, menu);
		return true;
	}

	@Override
	protected Fragment createFragment() {
		return HeadlinesFragment.newInstance();
	}

}
