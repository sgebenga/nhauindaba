package com.nhauindaba.app.ui;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class HeadlinesFragment extends ListFragment {
	private List<Article> articles;

	public static HeadlinesFragment newInstance() {
		return new HeadlinesFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(false);
		articles = ArticleFactory.getArticles();
		ArticleAdapter adapter = new ArticleAdapter(articles);
		setListAdapter(adapter);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.headlines_fragment, menu);
	}

	private class ArticleAdapter extends ArrayAdapter<Article> {

		public ArticleAdapter(List<Article> articles) {
			super(getActivity(), android.R.layout.simple_list_item_1, articles);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.headlines_list_item, null);
			}
			Article article = getItem(position);

			TextView titleTextView = (TextView) convertView
					.findViewById(R.id.headlines_list_item_titleTextView);
			titleTextView.setText(article.getTitle());
			TextView bodyTextView = (TextView) convertView
					.findViewById(R.id.headlines_list_item_bodyTextView);
			bodyTextView.setText(article.getBody());
			return convertView;
		}

	}

}
